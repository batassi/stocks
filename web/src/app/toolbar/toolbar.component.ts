import { Component, OnInit, Input } from '@angular/core';
import { faSearch, faSync, faEye } from '@fortawesome/free-solid-svg-icons';
import { select, Store } from '@ngrx/store';
import { StocksDataService } from '../stocks-data.service';
import { AppState } from '../reducers';
import { toggleDashboard, setMarket } from '../actions/config.actions';
import { setMarkets } from '../actions/data.actions';
import { getCurrentMarket } from '../selectors/config.selectors';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  @Input() getStocks: () => void;
  faSearch = faSearch;
  faSync = faSync;
  faEye = faEye;
  markets = [];
  selectedMarket: string
  search: string;

  constructor(private data: StocksDataService, private store: Store<AppState>) {
    this.store.pipe(select(getCurrentMarket)).subscribe((market: string) => {
      this.selectedMarket = market;
    });

    this.data.getMarkets().subscribe((data: any) => {
      this.markets = data;      
      this.store.dispatch(new setMarkets(data));
      this.updateMarket();
    });
  }

  showDashboard(): void {
    this.store.dispatch(new toggleDashboard(true));    
  }

  updateMarket(): void {
    this.store.dispatch(new setMarket(this.selectedMarket));
  }

  refresh(): void {
    this.getStocks();
  }

  ngOnInit(): void { }
}