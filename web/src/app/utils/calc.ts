/**
 * Calculations Utils
 */

export const valueChange = (last: number, open: number): number => {
    return Math.round((last - open) * 100) / 100;
};

export const percentChange = (last: number, open: number): number => {
    const diff = last - open;
    const percentage = (diff * 100) / open;
    return Math.round(percentage * 100) / 100;
};