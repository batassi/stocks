import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ChartModule } from 'angular-highcharts';
import { Chart } from 'highcharts';

export class DataPoint {
  last: number;
  date: string;
};

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: [
    '../../../node_modules/highcharts/css/themes/dark-unica.css',
    './chart.component.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ChartComponent implements OnInit {
  @Input() data: [] = [];
  chart: Chart;

  constructor() { }

  ngOnInit(): void {
    if (this.data) {
      const numbers = this.data.map((d: DataPoint) => [new Date(d.date).getTime(), d.last]);

      this.chart = new Chart('chartComponent', {
        chart: {
          type: 'line'
        },
        title: {
          text: ''
        },
        credits: {
          enabled: false
        },
        xAxis: {
          type: 'datetime'
        },
        legend: {
          enabled: false
        },
        plotOptions: {
          line: {
            marker: { 
              enabled: false
            }
          }
        },
        series: [
          {
            type: 'line',
            name: 'Value',
            data: numbers
          }
        ]
      });
    }
  }
}
