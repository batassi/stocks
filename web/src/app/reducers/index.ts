import {
  Action,
  ActionReducerMap,
  createReducer,
  MetaReducer,
  on
} from '@ngrx/store';
import { CustomAction } from '../actions/action';
import * as ConfigActions from '../actions/config.actions';
import * as DataActions from '../actions/data.actions';
import { environment } from '../../environments/environment';

export interface configState {
  currentMarket: string,
  isDashboardOpened: boolean
}

const initialConfigState: configState = {
  currentMarket: 'XNAS',
  isDashboardOpened: false
};

export interface dataState {
  markets: [],
  tickers: [],
  watchList: string[]
}

const initialDataState: dataState = {
  markets: [],
  tickers: [],
  watchList: ['AAPL', 'MSFT']
}

export interface AppState {
  config: configState,
  data: dataState
}

export function configReducer(state: configState = initialConfigState, action: CustomAction): configState {
  switch (action.type) {
    case ConfigActions.types.TOGGLE_DASHBOARD:
      return {
        ...state,
        isDashboardOpened: action.payload
      };
    case ConfigActions.types.SET_MARKET:
      return {
        ...state,
        currentMarket: action.payload
      };
    default:
      return state;
  }
}

export function dataReducer(state: dataState = initialDataState, action: CustomAction): dataState {
  switch(action.type) {
    case DataActions.types.SET_MARKETS:
      return {
        ...state,
        markets: action.payload
      };
    case DataActions.types.SET_TICKERS:
      return {
        ...state,
        tickers: action.payload
      };
    case DataActions.types.ADD_TICKER_TO_WATCHLIST: {
      const watchList = state.watchList;
      watchList.push(action.payload);
      return {
        ...state,
        watchList
      };
    }
    case DataActions.types.ADD_TICKERS_TO_WATCHLIST: {
      var watchList = state.watchList;
      watchList = watchList.concat(action.payload);
      return {
        ...state,
        watchList
      };
    }
    case DataActions.types.REMOVE_TICKER_FROM_WATCHLIST: {
      const watchList = state.watchList.filter(ticker => ticker !== action.payload);
      return {
        ...state,
        watchList
      };
    }
    default:
      return state
  }
}

export const reducers: ActionReducerMap<AppState> = {
  config: configReducer,
  data: dataReducer
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];