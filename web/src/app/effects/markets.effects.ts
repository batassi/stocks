import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as ConfigActions from '../actions/config.actions';
import * as DataActions from '../actions/data.actions';
import { StocksDataService } from '../stocks-data.service';
import { AppState } from '../reducers';

export interface Market {
  tickers: []
}

@Injectable()
export class MarketsEffects {

  @Effect()
  ConfigActions = this.actions$
    .pipe(
      ofType<ConfigActions.setMarket>(ConfigActions.types.SET_MARKET),
      mergeMap((action) => this.stockService.getTickers(action.payload)
      .pipe(
        map((data: Market) => {
          return (new DataActions.setTickers(data.tickers))
        })
      ))
    );

  constructor(private actions$: Actions, private store: Store<AppState>, private stockService: StocksDataService) {}
}