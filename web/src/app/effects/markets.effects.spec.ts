import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { MarketsEffects } from './markets.effects';

describe('MarketsEffects', () => {
  let actions$: Observable<any>;
  let effects: MarketsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MarketsEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(MarketsEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
