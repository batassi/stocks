import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AgGridModule } from 'ag-grid-angular';
import { ChartModule } from 'angular-highcharts';

import { AppComponent } from './app.component';
import { GlobalService } from './global';
import { NavbarComponent } from './navbar/navbar.component';
import { ClockComponent } from './clock/clock.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { MarketsEffects } from './effects/markets.effects';
import { GridComponent } from './grid/grid.component';
import { ActionCellComponent } from './grid/action-cell.component';
import { ConfirmationComponent } from './modals/confirmation/confirmation.component';
import { ListComponent } from './modals/list/list.component';
import { DetailsComponent } from './modals/details/details.component';
import { ChartComponent } from './chart/chart.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ClockComponent,
    ToolbarComponent,
    WatchlistComponent,
    DashboardComponent,
    GridComponent,
    ActionCellComponent,
    ConfirmationComponent,
    ListComponent,
    DetailsComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    ChartModule,
    AgGridModule.withComponents(),
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([MarketsEffects])
  ],
  providers: [GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule { }