import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { isDashboardOpened } from '../app/selectors/config.selectors';
import { AppState } from '../app/reducers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Stocks';
  isDashboardOpen: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.isDashboardOpen = this.store.pipe(select(isDashboardOpened));
  }
}
