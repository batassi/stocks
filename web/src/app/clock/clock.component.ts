import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { faClock } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {
  faClock = faClock;
  currentDateTime: string;

  constructor() {
    this.formatTime();
    this.setupTimer();
  }

  ngOnInit(): void {
  }

  formatTime(): void {
    this.currentDateTime = moment().format('ddd MMM D YYYY | hh:mm A');
  }

  setupTimer(): void {
    const $this = this;
    setInterval(function() {
      $this.formatTime();
    }, 1000);
  }
}
