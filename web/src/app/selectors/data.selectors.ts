import { AppState } from '../reducers';

export interface Market {
    mic: string,
    name: string
}

export interface Ticker {
    symbol: string,
    name: string
}

export const getMarkets = (state: AppState) => state.data.markets;

export const getCurrentMarketName = (state: AppState) => {
    const markhetObj: Market = state.data.markets.filter((market: Market) => market.mic === state.config.currentMarket)[0];
    return markhetObj.name;
}

export const getTickers = (state: AppState) => state.data.tickers;

export const getWatchList = (state: AppState) => {
    const list = state.data.watchList;
    return state.data.tickers.filter((t: Ticker) => state.data.watchList.indexOf(t.symbol) >= 0);
}