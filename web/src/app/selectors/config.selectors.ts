import { AppState } from '../reducers';

export const getCurrentMarket = (state: AppState) => state.config.currentMarket;

export const isDashboardOpened = (state: AppState) => state.config.isDashboardOpened;