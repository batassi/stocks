import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { Store, select } from '@ngrx/store';
import { AppState } from '../reducers';
import { toggleDashboard } from '../actions/config.actions';
import { Market, getCurrentMarketName } from '../selectors/data.selectors';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  faTimesCircle = faTimesCircle;
  marketName: Observable<string>;

  constructor(private store: Store<AppState>) { 
    this.marketName = this.store.pipe(select(getCurrentMarketName));
  }

  ngOnInit(): void {
  }

  hideDashboard(): void {
    this.store.dispatch(new toggleDashboard(false));
  }
}
