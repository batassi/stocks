import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { StocksDataService } from '../stocks-data.service';
import { AppState } from '../reducers';
import { Ticker, getWatchList } from '../selectors/data.selectors';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  data: [];

  constructor(private store: Store<AppState>, private dataService: StocksDataService) { 
    this.getStocks();
  }

  ngOnInit(): void {
  }

  getStocks(): void {
    this.store.pipe(select(getWatchList)).subscribe((tickers: Ticker[]) => {
      this.data = [];
      const symbols: string = tickers.map((t: Ticker) => t.symbol).join(',');

      if (symbols) {
        this.dataService.getStocks(symbols).subscribe((data: any) => {
          this.data = data.map((t:any) => ({
            ...t,
            name: tickers.filter((ticker: Ticker) => ticker.symbol === t.symbol)[0].name
          }));
        });
      }
    });
  }

}
