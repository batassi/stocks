import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { faTrashAlt, faBan } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { removeTickerFromWatchList } from '../../actions/data.actions';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: [
    '../modal.css',
    './confirmation.component.css',
  ],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmationComponent implements OnInit {
  @Input() symbol: string;

  faTrashAlt = faTrashAlt;
  faBan = faBan;
  closeResult = '';

  constructor(private modalService: NgbModal, private store: Store<AppState>) {}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  removeTicker(): void {
    this.store.dispatch(new removeTickerFromWatchList(this.symbol));
  }

  ngOnInit(): void {}
}
