import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { faSearch, faPlus, faBan } from '@fortawesome/free-solid-svg-icons';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../reducers';
import { Ticker, getTickers } from '../../selectors/data.selectors';
import { addTickersToWatchList } from '../../actions/data.actions';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: [
    '../modal.css',
    './list.component.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {
  @Input() symbols: string;

  faSearch = faSearch;
  faBan = faBan;
  faPlus = faPlus;
  tickers: Ticker[];
  list: string[] = [];
  closeResult = '';

  constructor(private modalService: NgbModal, private store: Store<AppState>) {}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {}

  lookupTickers(): void {
    const search = this.symbols.split(',');

    this.store.pipe(select(getTickers)).subscribe((data: Ticker[]) => {
      this.tickers = data.filter((t: Ticker) => {
        const symbol = t.symbol.indexOf('.') >= 0 ? t.symbol.substring(0, t.symbol.indexOf('.')) : t.symbol;  
        return search.indexOf(symbol) >= 0;
      });
    });
  }

  updateList(event): void {
    const { id, checked} = event.target;
    if(checked) {
      this.list.push(id);
    } else {
      this.list.splice(this.list.indexOf(id), 1);
    }
  }

  addTickers(): void {
    if (this.list.length > 0) {
      this.store.dispatch(new addTickersToWatchList(this.list));
      this.modalService.dismissAll();
    }
  }
}