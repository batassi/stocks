import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { faExpandArrowsAlt, faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { StocksDataService } from '../../stocks-data.service';
import * as calcs from '../../utils/calc';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: [
    '../modal.css',
    './details.component.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class DetailsComponent implements OnInit {
  @Input() symbol: string;
  title: string;
  change: number;
  percentChange: number;
  chartData: any;
  isOpen: boolean = false;
  details: {
    name: string,
    symbol: string,
    last: number,
    open: number,
    high: number,
    low: number,
    volume: number
  };
  colorClass = 'row';
  closeResult = '';

  faExpandArrowsAlt = faExpandArrowsAlt;
  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;

  constructor(private modalService: NgbModal, private store: Store<AppState>, private dataService: StocksDataService) { }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.isOpen = true;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {
    if(this.symbol) {
      this.dataService.getStocks(this.symbol).subscribe((data: any) => {
        this.details = data[0];
        this.title = this.details.name + ' (' + this.details.symbol + ')';
        this.change = calcs.valueChange(this.details.last, this.details.open);
        this.percentChange = calcs.percentChange(this.details.last, this.details.open);
        this.colorClass = 'row' + (this.change < 0 ? ' red-row' : (this.change > 0 ? ' green-row': ''));
      });

      this.dataService.getHistorical(this.symbol).subscribe((data: any) => {
        this.chartData = data.intraday.filter(elem => elem.last !== null);
      });
    }
  }
}
