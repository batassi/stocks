import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { faExpandArrowsAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'change-cell',
    templateUrl: './action-cell.component.html',
    styleUrls: ['./action-cell.component.css']
})

export class ActionCellComponent implements ICellRendererAngularComp {
    faExpandArrowsAlt = faExpandArrowsAlt;
    params: {
        data: {
            symbol: string
        }
    };

    constructor() {}

    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }

    expand(): void {
        console.log('Expanding...');
    }
}