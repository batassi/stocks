import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import * as calcs from '../utils/calc';

@Component({
    selector: 'change-cell',
    templateUrl: './change-cell.component.html',
    styleUrls: ['./change-cell.component.css']
})

export class ChangeCellComponent implements ICellRendererAngularComp {
    showColor: boolean = false;
    percentage: boolean = false;

    params: {
        colDef: {
            cellRendererFrameworkParams: {
                showColor: boolean,
                percentage: boolean
            }
        },
        data: {
            last: number,
            open: number
        }
    };
    displayValue: number;
    colorClass: string = 'change-cell';

    constructor() {}

    agInit(params: any): void {
        this.params = params;
        if(this.params.colDef.cellRendererFrameworkParams) {
            this.showColor = this.params.colDef.cellRendererFrameworkParams.showColor || false;
            this.percentage = this.params.colDef.cellRendererFrameworkParams.percentage || false;
        }

        const { last, open } = this.params.data;
        this.displayValue = this.percentage ? calcs.percentChange(last, open) : calcs.valueChange(last, open);

        if (this.showColor && this.displayValue !== 0) {
            this.colorClass += this.displayValue < 0 ? ' red-cell' : ' green-cell'
        }
    }

    refresh(): boolean {
        return false;
    }
}