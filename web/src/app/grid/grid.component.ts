import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { ChangeCellComponent } from './change-cell.component';
import { ActionCellComponent } from './action-cell.component';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: [
    '../../../node_modules/ag-grid-community/dist/styles/ag-grid.css',
    '../../../node_modules/ag-grid-community/dist/styles/ag-theme-alpine-dark.css',
    './grid.component.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class GridComponent implements OnInit {
  @Input() data: [];
  rowData = [];
  domLayout = "autoHeight";

  private gridApi: any;

  headers = [
    { headerName: 'Name', field: 'name', sortable: true },
    { headerName: 'Symbol', field: 'symbol', sortable: true },
    { headerName: 'Value', field: 'last', sortable: true },
    { headerName: 'Change', field: 'change', sortable: true, cellRendererFramework: ChangeCellComponent },
    { headerName: 'Change %', field: 'percent', sortable: true, cellRendererFramework: ChangeCellComponent, cellRendererFrameworkParams: { percentage: true, showColor: true }},
    { headerName: 'Open', field: 'open', sortable: true },
    { headerName: 'High', field: 'high', sortable: true },
    { headerName: 'Low', field: 'low', sortable: true },
    { headerName: 'Volume', field: 'volume', sortable: true },
    { headerName: '', field: 'actions', cellRendererFramework: ActionCellComponent }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.rowData = this.data;
  }

  onGridReady (params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }
}
