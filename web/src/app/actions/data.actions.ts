import { identifierModuleUrl } from '@angular/compiler';
import { CustomAction } from './action';

export const types = {
    SET_MARKETS: 'SET_MARKETS',
    SET_TICKERS: 'SET_TICKERS',
    ADD_TICKER_TO_WATCHLIST: 'ADD_TICKER_TO_WATCHLIST',
    ADD_TICKERS_TO_WATCHLIST: 'ADD_TICKERS_TO_WATCHLIST',
    REMOVE_TICKER_FROM_WATCHLIST: 'REMOVE_TICKER_FROM_WATCHLIST' 
};

export class setMarkets implements CustomAction {
    readonly type = types.SET_MARKETS;
    constructor(readonly payload: []) {}  
}

export class setTickers implements CustomAction {
    readonly type = types.SET_TICKERS;
    constructor(readonly payload: []) {}
}

export class addTickerToWatchList implements CustomAction {
    readonly type = types.ADD_TICKER_TO_WATCHLIST;
    constructor(readonly payload: string) {}
}

export class addTickersToWatchList implements CustomAction {
    readonly type = types.ADD_TICKERS_TO_WATCHLIST;
    constructor(readonly payload: string[]) {}
}

export class removeTickerFromWatchList implements CustomAction {
    readonly type = types.REMOVE_TICKER_FROM_WATCHLIST;
    constructor(readonly payload: string) {}
}