import { CustomAction } from './action';

export const types = {
  SET_MARKET: 'setMarket',
  TOGGLE_DASHBOARD: 'toggleDashboard'
};

export class setMarket implements CustomAction {
  readonly type = types.SET_MARKET;
  constructor(readonly payload: string) {}  
}

export class toggleDashboard implements CustomAction {
  readonly type = types.TOGGLE_DASHBOARD;
  constructor(readonly payload: boolean) {}
}