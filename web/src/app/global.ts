import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {
    public apiBasePath =  '/api/'; //'http://localhost:4000/';
    public isDashboardOpen = false;
    public defaultMarket = 'XNAS';

    public watchlist = [];

    public markets = [];
};