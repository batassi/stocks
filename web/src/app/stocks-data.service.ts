import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs"
import { GlobalService } from './global';

// import markets from './mock/markets.json';
// import tickers from './mock/tickers.json';
// import stocks from './mock/stocks.json';
// import historical from './mock/historical.json';

@Injectable({
  providedIn: 'root'
})
export class StocksDataService {
  constructor(private http: HttpClient, private global: GlobalService) { }

  public getMarkets () {
    const url = this.global.apiBasePath + 'exchanges';
    const headers = { 'Content-Type': 'Application/json', 'Accept': '*', 'Access-Control-Allow-Origin': '*' };
    return this.http.get(url, { headers });
    // return new Observable(function (observer) {
    //   const marketsArray = Object.keys(markets).map(mic => markets[mic]);  
    //   observer.next(marketsArray);
    // });
  }

  public getTickers (mic: string) {
    const url = this.global.apiBasePath + 'exchanges/' + mic;
    const headers = { 'Content-Type': 'Application/json', 'Accept': '*', 'Access-Control-Allow-Origin': '*' };
    return this.http.get(url, { headers });
    // return new Observable(function (observer) {
    //   const ts = tickers[mic];
    //   observer.next(ts);
    // });
  }

  public getStocks (symbols: string) {
    const url = this.global.apiBasePath + 'stocks/data?symbols=' + symbols;
    const headers = { 'Content-Type': 'Application/json', 'Accept': '*', 'Access-Control-Allow-Origin': '*' };
    return this.http.get(url, { headers });
    // return new Observable(function (observer) {
    //   const symbolsList = symbols.split(',');
    //   const stcks = stocks.filter(s => symbolsList.indexOf(s.symbol) >= 0);
    //   observer.next(stcks);
    // });
  }

  public getHistorical (symbol: string) {
    const url = this.global.apiBasePath + 'stocks/' + symbol;
    const headers = { 'Content-Type': 'Application/json', 'Accept': '*', 'Access-Control-Allow-Origin': '*' };
    return this.http.get(url, { headers });
    // return new Observable(function (observer) {
    //   observer.next(historical);
    // });
  }
}
