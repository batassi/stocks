var express = require('express');
var router = express.Router();

/* GET Watchlist. */
router.get('/:userid', function(req, res, next) {
  res.send('TODO: Should return Watchlist for given userid');
});

/* POST Add to Watchlist. */
router.post('/:userid/add', function(req, res, next) {
  res.send('TODO: Should add given stock to Watchlist for given userid');
});

/* DELETE Add to Watchlist. */
router.delete('/:userid/remove', function(req, res, next) {
  res.send('TODO: Should remove stock from Watchlist for given userid');
});

module.exports = router;