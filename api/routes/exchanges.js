var express = require('express');
var router = express.Router();
const api = require('../marketstack');

/* GET list of exchanges */
router.get('/', async function(req, res, next) {
	await api.fetch('exchanges')
    .then(response => {
      res.send(response.data.data);
    })
    .catch(error => {
      console.log(error);
      res.error(error);
    });
});

/* GET tickers for a given exchange by mic */
router.get('/:mic', async function(req, res, next) {
  const endPoint = 'exchanges/' + req.params.mic + '/tickers';
  await api.fetch(endPoint)
    .then(response => {
      res.send(response.data.data);
    })
    .catch(error => {
      console.log(error);
      res.error(error);
    });
});

module.exports = router;