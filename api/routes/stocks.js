var express = require('express');
var router = express.Router();
const api = require('../marketstack');

/* GET find stock(s) by name or symbols */
router.get('/', async function(req, res, next) {
  await api.fetch('tickers', req.query)
    .then(response => {
      res.send(response.data.data);
    })
    .catch(error => {
      console.log(error);
      res.error(error);
    });
});

/* GET latest stocks data by symbols */
router.get('/data', async function(req, res, next) {
  await api.fetch('intraday/latest', req.query)
    .then(response => {
      res.send(response.data.data);
    })
    .catch(error => {
      console.log(error);
      res.error(error);
    });
});

/* GET details for a given symbol */
router.get('/:symbol', async function(req, res, next) {
  const endPoint = 'tickers/' + req.params.symbol + '/intraday';
  await api.fetch(endPoint)
    .then(response => {
      res.send(response.data.data);
    })
    .catch(error => {
      console.log(error);
      res.error(error);
    });
});

module.exports = router;