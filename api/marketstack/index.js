const axios = require('axios');
const config = require('./config.json');
const apiKey = '?access_key=' + config.apiKey;

const serializeParams = (params) => {
    var urlParams = '';

    for(key in params) {
        urlParams += '&' + key + '=' + params[key];
    }

    return urlParams;
};

exports.fetch = (endPoint, params) => {
    return axios.get(config.basePath + endPoint + apiKey + serializeParams(params));
};